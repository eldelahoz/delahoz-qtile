#-*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen, KeyChord
from libqtile.command import lazy
from libqtile import bar, layout, widget
from libqtile.lazy import lazy
from libqtile.widget import Notify, PulseVolume

from typing import List  # noqa: F401
mod = "mod4"
terminal = "alacritty"

keys = [
    # Switch between windows
    Key([mod], "h",
        lazy.layout.left(),
        desc="Move focus to left"
        ),
    Key([mod], "l",
        lazy.layout.right(),
        desc="Move focus to right"
        ),
    Key([mod], "j",
        lazy.layout.down(),
        desc="Move focus down"
        ),
    Key([mod], "k",
        lazy.layout.up(),
        desc="Move focus up"
        ),
    Key([mod], "space",
        lazy.layout.next(),
        desc="Move window focus to other window"
        ),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h",
        lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        desc="Grow window up"),
    Key([mod], "n",
        lazy.layout.normalize(),
        desc="Reset all window sizes"),
    Key([mod], "m",
        lazy.layout.maximize(),
        desc="toggle zimdow between minimun and maximum sizes"),
    Key([mod], "f",
        lazy.window.toggle_fullscreen(),
        desc="toggle fullscreen"),
    Key([mod, "shift"], "f",
        lazy.window.toggle_floating(),
        desc="toggle floating"),




    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return",
        lazy.spawn(terminal),
        desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab",
        lazy.next_layout(),
        desc="Toggle between layouts"),

    Key([mod, "shift"], "Tab",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc="Switch which side main pane occupies (XmonadTall)"),

    Key([mod], "w",
        lazy.window.kill(),
        desc="Kill focused window"),

    Key([mod, "control"], "r",
        lazy.restart(),
        desc="Restart Qtile"),
    Key([mod, "control"], "q",
        lazy.shutdown(),
        desc="Shutdown Qtile"),
   Key([mod], "r",
       lazy.spawn("dmenu_run -p 'Run: '"),
       desc="Spawn a command using a prompt widget"),

    # == volumes
    Key([], "XF86AudioRaiseVolume",
        lazy.spawn("pulseaudio-ctl up")),
    Key([], "XF86AudioLowerVolume",
        lazy.spawn("pulseaudio-ctl down")),
    Key([], "XF86AudioMute",
        lazy.spawn("pulseaudio-ctl mute")),

    # == backlight
    Key([], "XF86MonBrightnessUp",
        lazy.spawn("light -A 5")),
    Key([], "XF86MonBrightnessDown",
        lazy.spawn("light -U 5")),
]

group_names = [("TER", {'layout': 'columns'}),
               ("FIR", {'layout': 'columns'}),
               ("DEV", {'layout': 'columns'}),
               ("MUS", {'layout': 'monadtall'}),
               ("GTF", {'layout': 'floating'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))



layout_theme = {"border_width": 2,
                "margin": 3,
                "border_focus": "11F04C",
                "border_normal": "084017"
               }


layouts = [
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
    layout.Columns(**layout_theme),
    layout.Max(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Stack(num_stacks=2),
    layout.RatioTile(**layout_theme),
    layout.Floating(**layout_theme),
    layout.TreeTab(
        font = "Ubuntu",
        fontsize = 13,
        sections = ["PREMIER", "DEUXIEME"],
        section_fontsize = 10,
        bg_color = "084017",
        active_bg = "366131",
        active_fg = "601212",
        panel_width = 100
        )
]


#colors = [["14260A", "14260A"], #Backgroun
#          ["3A4037", "3A4037"],
#          ["FFFFFF", "FFFFFF"],
#          ["F04705", "F04705"]
#    ]
colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#02F54B", "#02F54B"], # border line color for current tab
          ["#0DFC8F", "#0DFC8F"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#119df0", "#119df0"], # color for the 'even widgets'
          ["#14BDF5", "#14BDF5"], # window name
          ["#828282", "#828282"]] # backbround for inactive screens

#'',
prompt = "{0}@{1}: ".format(*os.environ["USER"], socket.gethostname())

widget_defaults = dict(
    font='Ubuntu',
    fontsize=12,
    padding=3,
    backgroun=colors[2]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                    linewidth = 0,
                    padding = 6,
                    foreground = colors[2],
                    background = colors[0]
                    ),
                widget.Image(
                    filename = "~/.config/qtile/icons/python.png",
                    scale = "False",
                    mouse_callbaks = {'Button1': lambda: qtile.cmd_spawn(terminal)}
                    ),
                widget.Sep(
                    linewidth = 0,
                    padding = 6,
                    foreground = colors[2],
                    background = colors[0]
                    ),
                widget.GroupBox(
                    font = "Ubuntu Bold",
                    fontsize = 10,
                    margin_y = 3,
                    margin_x = 0,
                    padding_y = 5,
                    padding_x = 3,
                    borderwidth = 3,
                    active = colors[2],
                    inactive = colors[7],
                    rounded = False,
                    highlight_color = colors[1],
                    highlight_method = "line",
                    this_current_screen_border = colors[6],
                    foreground = colors[2],
                    background = colors[0]
                    ),
                widget.Prompt(
                       prompt = prompt,
                       font = "Ubuntu Mono",
                       padding = 10,
                       foreground = colors[3],
                       background = colors[1]
                       ),
                widget.Sep(
                       linewidth = 0,
                       padding = 40,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.Sep(
                    linewidth = 0,
                    padding = 6,
                    foreground = colors[2],
                    background = colors[0]
                    ),
                widget.WindowName(
                    background = colors[0],
                    foreground = colors[2]
                    ),
                
                widget.Systray(
                    background = colors[0],
                    padding = 5
                    ),
                widget.Sep(
                    linewidth = 0,
                    padding = 6,
                    foreground = colors[2],
                    background = colors[0]
                    ),
              widget.TextBox(
                       text = '',
                       background = colors[0],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 32
                       ),
                widget.Net(
                    foreground = colors[2],
                    background = "119df0",
                    format = '{down} ↓↑ {up}'
                    ),
                widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 30
                       ),
                widget.ThermalSensor(
                        foreground = colors[2],
                        background = colors[5],
                        threshold = 90,
                        padding = 5
                        ),
                widget.TextBox(
                       text='',
                       background = colors[5],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 30
                       ),
                widget.TextBox(
                       text = " ⟳",
                       padding = 2,
                       foreground = colors[2],
                       background = "119df0",
                       fontsize = 14
                       ),
                widget.CheckUpdates(
                       update_interval = 1800,
                       distro = "Arch_checkupdates",
                       display_format = "{updates} Updates",
                       foreground = colors[2],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + ' -e sudo pacman -Syu')},
                       background = "119df0",
                       no_update_string = "N/A"
                       ),
                widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 32
                       ),
                widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = "000000",
                       background = colors[4],
                       padding = 0,
                       scale = 0.7
                       ),
                widget.CurrentLayout(
                       foreground = "000000",
                       background = colors[4],
                       padding = 5
                       ),
                widget.TextBox(
                       text = '',
                       background = colors[4],
                       foreground = colors[5],
                       padding = 0,
                       fontsize = 32
                       ),
                widget.Clock(
                       foreground = colors[2],
                       background = colors[5],
                       format='%A, %B %d - %H:%M'
                       ),
                widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       padding = 0,
                       fontsize = 32
                       ),
                widget.Battery(
                       foreground = "000000",
                       background = colors[4],
                       format=" {char} {percent:2.0%} ",
                       charge_char="⚡", discharge_char="🔋",
                       full_char="⚡", unknown_char="⚡",
                       empty_char="⁉️ ", update_interval=2,
                       show_short_text=False,
                       default_text=""
                        ),
                widget.Backlight(
                        format = 'Lgt: {percent:2.0%}',
                        backlight_name='intel_backlight',
                        update_interval=1,
                        foreground = "000000",
                        background = colors[4],
                        ),
                        
                Notify(fmt=" 🔥 {} "),
                #widget._pulse_audio(volume_app="pavucontrol"),
                PulseVolume(
                        foreground = "000000",
                        background = colors[4],
                        fmt = 'Vol {}',
                        volume_app="pavucontrol"
                        ),
                        
                #widget.QuickExit(),
                
            ],
            20, #Size
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
